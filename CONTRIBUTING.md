# Contributing

When contributing to this repository, please first discuss the change you wish to make via [Issue](/../issues) before making a change.

## Code of Conduct

This project adheres to the Contributor Covenant [code of conduct](/code-of-conduct.md).
By participating, you are expected to uphold this code.

## Open issue

Before raising an issue to this project, please read the [FAQ](/FAQ.md).
Then, please search in the [issue tracker](/../issues) for similar entries before submitting your own,
there's a good chance somebody else had the same issue or feature proposal.

Please submit bugs using the 'Bug' issue template provided on the issue tracker.
The text in the parenthesis is there to help you with what to include.

## Development

Please read [dev.md](/doc/dev.md) for details.

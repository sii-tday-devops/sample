# FAQ

## Docker

<details>
<summary>Authentication error</summary>

Sample error:

```bash
ERROR: denied: access forbidden
```

To solve the problem, you need to log in to GitLab's Container Registry using your GitLab username with the following command:

```bash
docker login registry.gitlab.com
```

</details>

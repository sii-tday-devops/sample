# Development

## Requirements

Please read [requirements.md](requirements.md) for details.

## Building from sources

You don’t need to build from source to use SII Boot (binaries in repo),
but if you want to try out the latest and greatest, SII Boot can be easily built
with the [maven](requirements.md#maven) and [JDK](requirements.md#jdk).

```bash
mvn clean install -DskipTests
```

## Run tests

Complete tests:

```bash
mvn test
```

Test a specific service:

```bash
mvn test -pl services/myservice
```

Test a specific service with its dependencies:

```bash
mvn test -pl services/myservice -am
```

Test a specific library with all samples which use it:

```bash
mvn test -pl module/mylibrary -amd
```

## Code style guides

* [**Markdown**](http://www.cirosantilli.com/markdown-style-guide/)

* **Java**
  [Use Google style guide](https://google.github.io/styleguide/javaguide.html).

  You can configure your IDE with [XML configuration](https://raw.githubusercontent.com/google/styleguide/gh-pages/eclipse-java-google-style.xml).

  **TIP :** To auto format your JAVA code, you can run the followed command:

  ```bash
  mvn fmt:format
  ```

## Git Practices

Pull requests cannot be accepted if they contain merge commits.

Always do `git pull --rebase` and `git rebase` vs `git pull` or `git merge`.

Always create a new branch for each merge request to avoid intermingling different features or fixes on the same branch.

Always **squash** your commits before `push`.

# Requirements

## Usage

### Maven

You can download the latest version [here](https://maven.apache.org/download.cgi).

### JDK

You can download the `JDK` fot windows on the [Red Hat website](https://developers.redhat.com/products/openjdk/download/).

Supported version:

* `1.8`
* `11`

## Development

### Git

Download [latest version](https://git-scm.com/download/win).

**TIPS :**

* When the install process, select the option
  `Checkout as-is, commit Unix-style line endings`.
* Do not install Git as administrator.

### Docker

Download links:

* [Windows 7](https://download.docker.com/win/stable/DockerToolbox.exe)
* [Windows 10](https://hub.docker.com/editions/community/docker-ce-desktop-windows)
* [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
* [Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
* [CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)

__Note :__ You need to be administrator to install _Docker_

Configuration needed for *Windows* systems < **10**:

1. Create a *Docker* machine.

   ```bash
   docker-machine create default
   ```

2. Get the environment variables to link *Docker* to your virtual machine.

    ```bash
    docker-machine env default
    ```

    Result should be:

    ```bash
    export DOCKER_TLS_VERIFY="1"
    export DOCKER_HOST="tcp://192.168.99.100:2376"
    export DOCKER_CERT_PATH="C:\Users\myuser.SII\.docker\machine\machines\default"
    export DOCKER_MACHINE_NAME="default"
    export COMPOSE_CONVERT_WINDOWS_PATHS="true"
    # Run this command to configure your shell:
    # eval $("C:\Program Files\Docker Toolbox\docker-machine.exe" env default)
    ```

3. Add all the previous environment variables to your system (as User).

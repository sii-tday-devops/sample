package app.module.core;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;


@RestController
@Slf4j
public class JsonErrorController extends AbstractErrorController {

  protected ErrorAttributes errorAttributes;

  public JsonErrorController(ErrorAttributes errorAttributes) {
    super(errorAttributes);
  }

  @Override
  public String getErrorPath() {
    return "/error";
  }

  @RequestMapping("/error")
  public Map<String, Object> error(HttpServletRequest request, WebRequest wRequest) {
    log.error("Error page {}", super.getErrorAttributes(request, true));

    return super.getErrorAttributes(request, false);
  }
}

package app;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

public interface EstablishmentRepository extends CrudRepository<Establishment, UUID> {

  Establishment findOneById(UUID id);
}

import http from './http'

export default {

  async create (comment) {
    try {
      const { data } = await http.post(`/comments`, comment)
      return data
    } catch (e) {
      return null
    }
  },

  async deleteAll (establishmentId) {
    try {
      const { status } = await http.delete(`/comments?establishment=${establishmentId}`)
      return status === 200
    } catch (e) {
      return null
    }
  }

}

import http from './http'

export default {

  async getAll () {
    const { data: establishments } = await http.get('/establishments')

    for (let establishment of establishments) {
      establishment.pictures = await this.getPictures(establishment.id)
      establishment.comments = await this.getComments(establishment.id)
    }
    return establishments
  },

  async getById (establishmentId) {
    const { data: establishment } = await http.get(`/establishments/${establishmentId}`)
    if (establishment) {
      establishment.pictures = await this.getPictures(establishment.id)
      establishment.comments = await this.getComments(establishment.id)
    }
    return establishment
  },

  async getPictures (establishmentId) {
    try {
      const { data: pictures } = await http.get(`/pictures?establishment=${establishmentId}`)
      return pictures
    } catch (e) {
      return []
    }
  },

  async getComments (establishmentId) {
    try {
      const { data: comments } = await http.get(`/comments?establishment=${establishmentId}`)
      return comments
    } catch (e) {
      return []
    }
  },

  async create (establishment) {
    const { data } = await http.post(`/establishments`, establishment)
    data.comments = []
    data.pictures = []
    return { ...establishment, ...data }
  },

  async update (establishment) {
    const { data } = await http.put(`/establishments/${establishment.id}`, establishment)
    return { ...establishment, ...data }
  },

  async delete (establishment) {
    try {
      const { status } = await http.delete(`/establishments/${establishment.id}`)
      return status === 204
    } catch (e) {
      return null
    }
  }

}

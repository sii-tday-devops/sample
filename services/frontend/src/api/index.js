import comment from './comment'
import establishment from './establishment'
import picture from './picture'

export default {
  comment,
  establishment,
  picture
}

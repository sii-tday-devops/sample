import http from './http'

export default {

  async create (picture) {
    try {
      const { data } = await http.post(`/pictures`, picture)
      return data
    } catch (e) {
      return null
    }
  },

  async deleteAll (establishmentId) {
    try {
      const { status } = await http.delete(`/pictures?establishment=${establishmentId}`)
      return status === 200
    } catch (e) {
      return null
    }
  }

}

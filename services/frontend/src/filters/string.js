
export function truncate (text = '', length = 30, suffix = '...') {
  return text.slice(0, length) + (length < text.length ? suffix : '')
}

export function formatUrl (url = '') {
  return url && url.includes('//') ? url.split('//')[1] : url
}

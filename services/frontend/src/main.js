import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import './plugins/registerServiceWorker'
import App from './App.vue'
import router from './router'
import store from './store'
import * as filters from './filters'

Vue.config.productionTip = false

// Import filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

new Vue({
  router,
  store,
  render: h => h(App),
  mounted () {
    this.$store.dispatch('getEstablishments')
  }
}).$mount('#app')

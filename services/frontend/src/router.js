import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'establishments',
      component: () => import(/* webpackChunkName: "establishments" */ './views/establishments/List.vue')
    },
    {
      path: '/establishments/:id',
      name: 'establishments-details',
      component: () => import(/* webpackChunkName: "establishments" */ './views/establishments/Details.vue')
    },
    {
      path: '/settings',
      name: 'settings',
      component: () => import(/* webpackChunkName: "settings" */ './views/Settings.vue')
    },
    {
      path: '/404',
      name: 'error-404',
      component: () => import(/* webpackChunkName: "errors" */ './views/errors/404.vue')
    }
  ]
})

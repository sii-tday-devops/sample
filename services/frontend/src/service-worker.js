/* eslint-disable no-undef, semi */
workbox.setConfig({
  debug: true
});

workbox.skipWaiting();
workbox.clientsClaim();
workbox.precaching.precacheAndRoute(self.__precacheManifest || [], {});

workbox.routing.registerRoute(
  new RegExp('https://picsum.photos/320/240(.*)'),
  workbox.strategies.cacheFirst({
    cacheName: 'picsum',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50,
        maxAgeSeconds: 5 * 24 * 60 * 60 // 5 jours
      })
    ]
  })
);

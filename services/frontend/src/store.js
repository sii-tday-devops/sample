import Vue from 'vue'
import Vuex from 'vuex'
import moment from 'moment'
import api from '@/api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    establishments: []
  },
  getters: {
    allEstablishments: (state) => state.establishments,
    getById: (state, id) => state.establishments.find(e => e.id === id),
    getMonths: (state) => state.establishments
      .reduce((months, establishment) => {
        const month = moment(establishment.createdAt).startOf('month').toISOString()
        return months.includes(month) ? months : [...months, month]
      }, [])
      .sort()
      .reverse()
  },
  mutations: {
    SET_ESTABLISHMENTS (state, establishments) {
      state.establishments = establishments
    },
    ADD_ESTABLISHMENT (state, establishment) {
      state.establishments = [...state.establishments, establishment]
    },
    UPDATE_ESTABLISHMENT (state, establishment) {
      const index = state.establishments.findIndex(e => e.id === establishment.id)
      state.establishments.splice(index, 1, establishment)
    },
    REMOVE_ESTABLISHMENT (state, id) {
      const index = state.establishments.findIndex(e => e.id === id)
      state.establishments.splice(index, 1)
    }
  },
  actions: {
    // Récupère les établissements depuis l'API, puis les stocke dans le store
    async getEstablishments ({ commit }) {
      const establishments = await api.establishment.getAll()
      commit('SET_ESTABLISHMENTS', establishments)

      return establishments
    },

    // Récupère un établissement depuis le store, sinon depuis l'API
    async getEstablishment ({ state }, id) {
      let establishment = state.establishments.find(e => e.id === id)

      if (!establishment) {
        establishment = await api.establishment.getById(id)
      }

      return establishment
    },

    // Met à jour un établissement depuis l'API, puis dans le store
    async saveEstablishment ({ commit }, data) {
      let establishment
      if (!data.id) {
        establishment = await api.establishment.create(data)
        commit('ADD_ESTABLISHMENT', establishment)
      } else {
        establishment = await api.establishment.update(data)
        commit('UPDATE_ESTABLISHMENT', establishment)
      }
      return establishment
    },

    // Supprime les données associés à l'établissement (pictures, comments, ...) puis l'établissement puis nettoie le store
    async removeEstablishment ({ commit }, establishment) {
      try {
        await api.comment.deleteAll(establishment.id)
        await api.picture.deleteAll(establishment.id)
        const success = await api.establishment.delete(establishment)

        if (success) {
          commit('REMOVE_ESTABLISHMENT', establishment.id)
        }
        return success
      } catch (e) {
        return false
      }
    },

    async addComment ({ commit, state }, data) {
      const comment = await api.comment.create(data)

      if (comment) {
        let establishment = state.establishments.find(e => e.id === comment.establishmentId)
        establishment.comments.push(comment)
        commit('UPDATE_ESTABLISHMENT', establishment)
      }
    },

    async addPicture ({ commit, state }, data) {
      const picture = await api.picture.create(data)

      if (picture) {
        let establishment = state.establishments.find(e => e.id === picture.establishmentId)
        establishment.pictures.push(picture)
        commit('UPDATE_ESTABLISHMENT', establishment)
      }
    }
  }
})

module.exports = {
  pwa: {
    name: 'DATW',
    themeColor: '#1565c0',
    msTileColor: '#FFFFFF',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'src/service-worker.js'
    }
  },
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://localhost:8080/',
        changeOrigin: true
      }
    },
    host: '51.83.76.19',
    port: 1234,
    disableHostCheck: true,
  }
}

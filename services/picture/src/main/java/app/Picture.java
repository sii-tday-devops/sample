package app;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
@Data
public class Picture {

  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  @Column(name = "id", updatable = false)
  @Type(type = "uuid-char")
  @ApiModelProperty(notes = "The generated ID", accessMode = AccessMode.READ_ONLY, hidden = true)
  @Setter(AccessLevel.NONE)
  private UUID id;

  @Column(nullable = false, length = 255)
  private String author;

  @Column(nullable = true, columnDefinition = "text")
  private String src;

  @Column(nullable = false, updatable = false)
  private UUID establishmentId;

  @Column(updatable = false)
  @ApiModelProperty(accessMode = AccessMode.READ_ONLY, hidden = true)
  private Date createdAt;

  @PrePersist
  void createdAt() {
    this.createdAt = new Date();
  }
}

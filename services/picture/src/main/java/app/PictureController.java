package app;

import java.util.UUID;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.NotAcceptableStatusException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping(path = "/api/pictures", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class PictureController {

  @Autowired private PictureRepository repository;

  @Autowired private RestTemplate template;

  @Value("${serviceUrlSuffix}")
  private String serviceSuffix;

  @Value("${server.port}")
  private String servicePort;

  @GetMapping
  public Iterable<Picture> list(@RequestParam("establishment") UUID establishmentId) {
    return repository.findByEstablishmentId(establishmentId);
  }

  @DeleteMapping
  public Long deleteAll(@RequestParam("establishment") UUID establishmentId) {
    return repository.deleteByEstablishmentId(establishmentId);
  }

  @PostMapping
  public ResponseEntity<Picture> create(
      @RequestBody Picture entity, UriComponentsBuilder ucBuilder) {
    log.trace("Creating new picture.");

    if (!isEstablishmentExist(entity.getEstablishmentId())) {
      throw new NotAcceptableStatusException("Establishment not found.");
    }

    try {
      repository.save(entity);

      HttpHeaders headers = new HttpHeaders();
      headers.setLocation(
          ucBuilder.path("/api/pictures/{id}").buildAndExpand(entity.getId()).toUri());

      log.info("Picture created with id {}.", entity.getId());

      return new ResponseEntity<Picture>(entity, headers, HttpStatus.CREATED);
    } catch (Exception e) {
      log.error("Unable to create picture.", e);
      throw new NotAcceptableStatusException("Unable to create picture.");
    }
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity<Picture> get(@PathVariable("id") UUID id) {
    Picture entity = repository.findOneById(id);

    if (entity == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Picture not found.");
    }

    return new ResponseEntity<Picture>(entity, HttpStatus.OK);
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<Picture> delete(@PathVariable("id") UUID id) {
    log.trace("Deleting picture {}.", id);

    Picture entity = repository.findOneById(id);

    if (entity == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Picture not found.");
    }

    repository.delete(entity);

    log.info("Picture with id {} deleted.", id);

    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  private Boolean isEstablishmentExist(UUID establishment) {
    String url = "http://establishment" + serviceSuffix + ":" + servicePort + "/api/establishments/" + establishment;
    Boolean retval = false;

    try {
      Object tmp = template.getForObject(url, Object.class);
      retval = true;
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
        throw e;
      }
    }

    return retval;
  }
}

package app;

import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

public interface PictureRepository extends CrudRepository<Picture, UUID> {

  Picture findOneById(UUID id);

  Iterable<Picture> findByEstablishmentId(UUID id);

  @Transactional
  Long deleteByEstablishmentId(UUID id);

}

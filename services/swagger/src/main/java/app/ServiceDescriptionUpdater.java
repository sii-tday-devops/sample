package app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class ServiceDescriptionUpdater {

  @Autowired private SwaggerConfigProperties swaggerConfigProperties;

  @Autowired private ServiceDefinitionsContext definitionContext;

  @Autowired private RestTemplate template;

  @Value("${serviceUrlSuffix}")
  private String serviceSuffix;

  @Value("${server.port}")
  private String servicePort;

  @Scheduled(fixedDelayString = "5000")
  public void refreshSwaggerConfigurations() {
    log.debug("Starting fetch swagger api docs");

    for (String service : swaggerConfigProperties.getServices()) {
      log.debug("Starting fetch swagger api docs for service {}", service);

      Optional<Object> jsonData = getSwaggerDefinitionForAPI(service);

      if (jsonData.isPresent()) {
        String content = getJSON(jsonData.get());
        definitionContext.addServiceDefinition(service, content);

        log.debug("Ressource updated for service {}", service);
      } else {
        log.error("Skipping service id : {} Error : Could not get Swagger definition from API ", service);
      }
    }
  }

  private Optional<Object> getSwaggerDefinitionForAPI(String service) {
    try {
      String url = "http://" + service + serviceSuffix + ":" + servicePort + "/v2/api-docs";
      Object jsonData = template.getForObject(url, Object.class);
      return Optional.of(jsonData);
    } catch (RestClientException e) {
      log.error("Error while getting service definition for service {}", service, e);
      return Optional.empty();
    }
  }

  public String getJSON(Object jsonData) {
    try {
      return new ObjectMapper().writeValueAsString(jsonData);
    } catch (JsonProcessingException e) {
      log.error("Error while reading documentation", e);
      return "";
    }
  }
}

package app;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Component
@ConfigurationProperties("documentation.swagger")
@Getter
@Setter
@ToString
public class SwaggerConfigProperties {

  private Integer refreshrate = 5000;

  private List<String> services = new ArrayList<>();
}
